﻿/*
 * @author Valentin Simonov / http://va.lent.in/
 */

using UnityEngine;
using TouchScript.Gestures;

namespace TouchScript.Examples.CameraControl
{
    public class CameraController : MonoBehaviour
    {
        public ScreenTransformGesture ManipulationGesture;
        public float RotationSpeed = 200f;

        private Transform pivot;
        private Transform cam;

        private bool isAnimating = false;
        private Transform spotRotatingTo;
        public float targetDistance;
        public float animationSpeed;

        void Update()
        {
            if(isAnimating)
            {
                cam.RotateAround(pivot.position, Vector3.up, animationSpeed);

                float distance = (cam.position - spotRotatingTo.position).magnitude;
                if (distance > targetDistance)
                {
                    animationSpeed = 30;
                }
                else
                {
                    animationSpeed = 0;
                    isAnimating = false;
                }
            }
        }

        public void RotateToSpot(GameObject spotObj)
        {
            isAnimating = true;
            spotRotatingTo = spotObj.transform;
        }

        private void Awake()
        {
            pivot = transform.Find("Pivot");
            cam = transform.Find("Pivot/Camera");
        }

        private void OnEnable()
        {
            ManipulationGesture.Transformed += manipulationTransformedHandler;
        }

        private void OnDisable()
        {
            ManipulationGesture.Transformed -= manipulationTransformedHandler;
        }

        private void manipulationTransformedHandler(object sender, System.EventArgs e)
        {
            var rotation = Quaternion.Euler(0,
                -ManipulationGesture.DeltaPosition.x/Screen.width*RotationSpeed,
                ManipulationGesture.DeltaRotation);
            pivot.localRotation *= rotation;
        }
    }
}