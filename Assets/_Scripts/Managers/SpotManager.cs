﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpotManager : MonoBehaviour
{

    public static SpotManager Instance { get; set; }

    public enum SpotEspecialization
    {
        None = 0,
        Barracks = 1,
        Farms = 2,
        Fortress = 3
    }

    public List<Spot> spotsList = new List<Spot>();
    private int totalSpots;
    public int currentOwnSpots, especializationMinTroops;
    public Spot currentSelectedSpot;
    public bool wasTapped;
    private Spot currentDestinationSpot;

    void Awake()
    {
        Instance = this;
        EventsManager.OnMapCreationEnd += StartPlayerSpot;
        EventsManager.OnGameTurnEnd += RefreshSpots;
        EventsManager.OnSpotChangeOwner += RefreshSpots;
        EventsManager.OnSpotUnselect += UnselectAllSpots;
        EventsManager.OnTravelStart += SpotTravelStart;
    }

    private void StartPlayerSpot()
    {
        int randomSpot = Random.Range(0, spotsList.Count - 1);
        ChangeSpotOwner(spotsList[randomSpot], PlayerManager.Instance.playerID);

        int randomEnemySpot = Random.Range(0, spotsList.Count - 1);
        while (randomEnemySpot == randomSpot)
            randomEnemySpot = Random.Range(0, spotsList.Count - 1);
        ChangeSpotOwner(spotsList[randomEnemySpot], PlayerManager.Instance.enemyID);
    }

    private void RefreshSpots()
    {
        totalSpots = 0;
        currentOwnSpots = 0;

        foreach (var spot in spotsList)
        {
            totalSpots++;
            if (spot.currentPlayerID == PlayerManager.Instance.playerID)
                currentOwnSpots++;
        }
    }

    public void SelectSpot(Spot selectedSpot)
    {
        foreach (var spot in spotsList)
        {
            if (spot == selectedSpot)
                spot.SelectSpot(true);
            else
                spot.SelectSpot(false);
        }

        currentSelectedSpot = selectedSpot;

        EventsManager.SpotSelect();
    }

    public void UnselectAllSpots()
    {
        foreach (var spot in spotsList)
            spot.UnselectSpot();

        currentSelectedSpot = null;
    }

    public void DestinationSpot(Spot destinationSpot)
    {
        if(destinationSpot != currentSelectedSpot)
        {
            currentDestinationSpot = destinationSpot;

            if(ResourcesManager.Instance.currentResources >= GameManager.Instance.resourcesToTravel)
            {
                if(currentSelectedSpot.spotTroops > 1)
                    EventsManager.TravelStart();
                else
                    HUDManager.Instance.SetFeedback("NEED MORE TROOPS");
            }
            else
            {
                HUDManager.Instance.SetFeedback("NEED MORE RESOURCES");
                Debug.LogWarning("NECESSARIOS MAIS RESOURCES");
            }
            EventsManager.SpotUnselect();
        }
    }

    private void SpotTravelStart()
    {
        TroopsManager.Instance.TroopsTravelStart(currentSelectedSpot, currentDestinationSpot, currentSelectedSpot.SpotTroopsToTravel(), currentSelectedSpot.currentPlayerID);

        currentSelectedSpot = null;
        currentDestinationSpot = null;
    }

    public void ChangeSpotOwner(Spot currentSpot, int playerID)
    {
        currentSpot.ChangeThisSpotOwner(playerID);
        EventsManager.SpotChangeOwner();
    }
}
