﻿using UnityEngine;
using System.Collections;

public class EventsManager : MonoBehaviour
{

    public delegate void EventsActions();

    public static event EventsActions OnMapCreationEnd;
    public static void MapCreationEnd()
    {
        if (OnMapCreationEnd != null)
            OnMapCreationEnd();
    }

    public static event EventsActions OnResourcesChanged;
    public static void ResourcesChanged()
    {
        if (OnResourcesChanged != null)
            OnResourcesChanged();
    }

    public static event EventsActions OnTroopsChanged;
    public static void TroopsChanged()
    {
        if (OnTroopsChanged != null)
            OnTroopsChanged();
    }

    public static event EventsActions OnTroopsGenerated;
    public static void TroopsGenerated()
    {
        if (OnTroopsGenerated != null)
            OnTroopsGenerated();
    }

    public static event EventsActions OnTroopsUpgraded;
    public static void TroopsUpgraded()
    {
        if (OnTroopsUpgraded != null)
            OnTroopsUpgraded();
    }

    public static event EventsActions OnEnemyTroopsUpgraded;
    public static void EnemyTroopsUpgraded()
    {
        if (OnEnemyTroopsUpgraded != null)
            OnEnemyTroopsUpgraded();
    }

    public static event EventsActions OnSelectDrag;
    public static void SelectDrag()
    {
        if (OnSelectDrag != null)
            OnSelectDrag();
    }

    public static event EventsActions OnSelectDragEnd;
    public static void SelectDragEnd()
    {
        if (OnSelectDragEnd != null)
            OnSelectDragEnd();
    }

    public static event EventsActions OnSpotSelect;
    public static void SpotSelect()
    {
        if (OnSpotSelect != null)
            OnSpotSelect();
    }

    public static event EventsActions OnSpotUnselect;
    public static void SpotUnselect()
    {
        if (OnSpotUnselect != null)
            OnSpotUnselect();
    }

    public static event EventsActions OnSpotChangeOwner;
    public static void SpotChangeOwner()
    {
        if (OnSpotChangeOwner != null)
            OnSpotChangeOwner();
    }

    public static event EventsActions OnSpotAttacked;
    public static void SpotAttacked()
    {
        if (OnSpotAttacked != null)
            OnSpotAttacked();
    }

    public static event EventsActions OnSpotUpgraded;
    public static void SpotUpgraded()
    {
        if (OnSpotUpgraded != null)
            OnSpotUpgraded();
    }

    public static event EventsActions OnTravelStart;
    public static void TravelStart()
    {
        if (OnTravelStart != null)
            OnTravelStart();
    }

    public static event EventsActions OnEnemyTravelStart;
    public static void EnemyTravelStart()
    {
        if (OnEnemyTravelStart != null)
            OnEnemyTravelStart();
    }

    public static event EventsActions OnTravelEnd;
    public static void TravelEnd()
    {
        if (OnTravelEnd != null)
            OnTravelEnd();
    }

    public static event EventsActions OnGameTurnEnd;
    public static void GameTurnEnd()
    {
        if (OnGameTurnEnd != null)
            OnGameTurnEnd();
    }
}
