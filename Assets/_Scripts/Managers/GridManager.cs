﻿using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

public class GridManager : MonoBehaviour {

    public static GridManager Instance { get; set; }
    public List<List<GameObject>> gridMatrix = new List<List<GameObject>>();

    public GameObject cellPrefab, pathPrefab;
    public int polesLimit;
    public float planetCircumference, gridSize;
    public int populatedMinCell, populatedMaxCell;
    private int _islandRange, _islandAmount, _spotAmount;

    public float rSize;
    float cellSize;
    float rotationAngle;

    public enum CellStates
    {
        Water = 0,
        Spot = 1,
        Land = 2
    }

    void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        _spotAmount = GameManager.Instance.spotsAmount;
        _islandAmount = GameManager.Instance.islandAmount;
        _islandRange = GameManager.Instance.islandRange;

        StartCoroutine(GenerateSphere());
    }

    IEnumerator GenerateSphere()
    {
        rSize = planetCircumference / (2*Mathf.PI);
        cellSize = planetCircumference/gridSize;
        rotationAngle = 360 / gridSize;

        Quaternion lastRotation = Quaternion.Euler(0,0,0);

        int cellCounter = 0;

        GameObject cellsContainer = new GameObject();
        cellsContainer.name = "Cells";

        for (int j = 0; j < gridSize; j++)
        {
            List<GameObject> columnList = new List<GameObject>();

            for (int i = 0; i < gridSize; i++)
            {

                if (i > gridSize / polesLimit && i < (gridSize / 2 - gridSize / polesLimit))
                {
                    GameObject currCell = GameObject.Instantiate(cellPrefab) as GameObject;
                    currCell.transform.localRotation = Quaternion.Euler(lastRotation.x + rotationAngle * i, lastRotation.y + rotationAngle * j, 0);
                    currCell.GetComponent<Cell>().SetCellState(CellStates.Water);

                    foreach (var model in currCell.GetComponent<Cell>().cellModels)
                    {
                        model.transform.localScale = Vector3.one * cellSize * 1.05f;
                        model.transform.localPosition = new Vector3(0, rSize, 0);
                    }
                    foreach (var model in currCell.GetComponent<Cell>().terrainModels)
                    {
                        model.transform.localScale = Vector3.one * cellSize * 1.05f;
                        model.transform.localPosition = new Vector3(0, rSize + 0.03f, 0);
                    }

                    currCell.GetComponent<Cell>().cellX = j;
                    currCell.GetComponent<Cell>().cellY = i;

                    lastRotation = Quaternion.Euler(currCell.transform.localRotation.x, currCell.transform.localRotation.y, currCell.transform.localRotation.z);

                    currCell.name = "Cell" + cellCounter;
                    cellCounter++;

                    currCell.transform.parent = cellsContainer.transform;
                    columnList.Add(currCell);
                }
            }
            gridMatrix.Add(columnList);
        }
        cellsContainer.transform.parent = transform;

        yield return null; // Faz com que o processo não estoure em memória no device, mas demora mais

        StartCoroutine(GenerateTerrain());

       // StartCoroutine(GenerateHexPositions());
    }

    IEnumerator GenerateHexPositions()
    {
        for (int i = 0; i < gridMatrix.Count; i++)
        {
            for (int j = 0; j < gridMatrix[i].Count; j++)
            {
                if (i % 2 == 0)
                {
                    gridMatrix[i][j].transform.Rotate(gameObject.transform.position, rotationAngle);
                    gridMatrix[i][j].transform.rotation = Quaternion.RotateTowards(gridMatrix[i][j+1].transform.rotation, gridMatrix[i][j].transform.rotation, rotationAngle/2);
                }
            }
            yield return null;
        }
    }

    IEnumerator GenerateTerrain()
    {
        int minRange = 1;
        int maxRange = (_islandRange / 2);

        for (int j = 0; j < _islandAmount; j++)
        {
           // yield return null;

            int islandCenterX = Random.Range(maxRange, gridMatrix.Count - maxRange);
            int islandCenterY = Random.Range(populatedMinCell, populatedMaxCell);
            int currentRange = minRange;
            bool isIncrementing = true;

            for (int i = islandCenterX - maxRange; i < islandCenterX + maxRange; i++) // i = X & k/l = Y
            {
                for (int k = islandCenterY; k > islandCenterY - currentRange; k--)
                {
                    GameObject currentCell = null;
                    if (k < gridMatrix[i].Count-1)
                    {
                        currentCell = gridMatrix[i][k];
                    }
                    else
                    {
                        currentCell = gridMatrix[i][gridMatrix[i].Count - 1];
                    }
                    

                    if (currentCell != null)
                    {
                        currentCell.GetComponent<Cell>().SetCellState(CellStates.Land);
                    }
                }

                for (int l = islandCenterY + 1; l < islandCenterY + currentRange; l++)
                {
                    GameObject currentCell = null;
                    if (l < gridMatrix[i].Count)
                    {
                        currentCell = gridMatrix[i][l];
                    }
                    else
                    {
                        currentCell = gridMatrix[i][gridMatrix[i].Count - 1];
                    }

                    if (currentCell != null)
                    {
                        currentCell.GetComponent<Cell>().SetCellState(CellStates.Land);
                    }
                }

                if (currentRange < maxRange && isIncrementing)
                {
                    currentRange++;
                }
                else
                {
                    isIncrementing = false;
                    currentRange--;
                }
            }
        }

        yield return null;

        StartCoroutine(GenerateSpots());
    }

    IEnumerator GenerateSpots()
    {
        List<Spot> spotsList = SpotManager.Instance.spotsList;

        GameObject spotsContainer = new GameObject();
        spotsContainer.name = "Spots";
        for (int i = 1; i < _spotAmount + 1; i++)
        {
            int randomX = Random.Range(1, gridMatrix.Count);
            int randomY = Random.Range(1, gridMatrix[randomX].Count);
            while (gridMatrix[randomX][randomY].GetComponent<Cell>().cellState != CellStates.Land)
            {
                randomX = Random.Range(1, gridMatrix.Count);
                randomY = Random.Range(1, gridMatrix[randomX].Count);
            }

            gridMatrix[randomX][randomY].GetComponent<Cell>().SetCellState(CellStates.Spot);

            GameObject currCell = gridMatrix[randomX][randomY];
            currCell.name = "Spot" + i.ToString();
            currCell.transform.parent = spotsContainer.transform;
            spotsList.Add(currCell.GetComponentInChildren<Spot>());
        }
        spotsContainer.transform.parent = transform;

        yield return null;

        SpotManager.Instance.spotsList = spotsList;
        EventsManager.MapCreationEnd();
    }

   /* public GameObject GeneratePathBetweenSpots(Spot fromSpot, Spot toSpot)
    {
		if(GameObject.Find("PathFrom" + fromSpot.name + "To" + toSpot.name))
		{
			GameObject.Find ("PathFrom" + fromSpot.name + "To" + toSpot.name).GetComponent<Path> ().troopsOnPath++;
			return null;
		}else{
			GameObject pathContainer = new GameObject();
			pathContainer.name = "PathFrom" + fromSpot.transform.parent.name + "To" + toSpot.transform.parent.name;
			pathContainer.AddComponent<Path>();
			
			Transform toSpotTransform = toSpot.gameObject.transform;
			Transform fromSpotTransform = fromSpot.gameObject.transform;
			
			cellSize = 0.1f;
			float lineSize = Quaternion.Angle(toSpotTransform.rotation, fromSpotTransform.rotation) / (cellSize * 50);
			rotationAngle = Quaternion.Angle(toSpotTransform.rotation, fromSpotTransform.rotation) / lineSize;
			
			StartCoroutine(PlaceDots(lineSize, toSpotTransform, fromSpotTransform, pathContainer.transform));
			
			return pathContainer;
		}
    }

    IEnumerator PlaceDots(float lineSize, Transform toSpotTransform, Transform fromSpotTransform, Transform container)
    {
        for (int i = 0; i < lineSize; i++)
        {
            Quaternion targetPath = Quaternion.Euler(toSpotTransform.parent.eulerAngles);
            GameObject currDot = GameObject.Instantiate(pathPrefab, Vector3.zero, fromSpotTransform.parent.rotation) as GameObject;
            currDot.transform.rotation = Quaternion.RotateTowards(fromSpotTransform.parent.rotation, targetPath, rotationAngle * i);
            currDot.GetComponentInChildren<MeshRenderer>().transform.localScale = Vector3.one * cellSize;
            currDot.GetComponentInChildren<MeshRenderer>().transform.localPosition = new Vector3(0, rSize + 0.05f, 0);
            currDot.transform.parent = container;

        }
            yield return null;
    }*/
}
