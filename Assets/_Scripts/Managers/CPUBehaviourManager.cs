﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CPUBehaviourManager : MonoBehaviour {

    public static CPUBehaviourManager Instance { get; set; }

    private int cpuTotalTroops;
    public int cpuTotalSpots;
    public int cpuTotalResources;
    private int resourcesCounter;
    private List<Spot> totalSpotList;
    private List<Spot> enemySpotList;

    public int cpuTroopsStrengthModifier = 0;
    public int cpuTroopsToTravelModifier = 1;
    public TroopsManager.TroopsEspecializations currentUpgrade;

    private bool isSpotSetupTurn = true;
    private bool isTroopsSetupTurn = true;
    private int actionSpotTurn;
    private int actionTroopsTurn;
    public int minCPUTurnInterval;
    public int maxCPUTurnInterval;
    public int minCPUTroopsUpgradeInterval;
    public int maxCPUTroopsUpgradeInterval;

    void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        EventsManager.OnMapCreationEnd += StartCPUGame;
        EventsManager.OnGameTurnEnd += RefreshSpotsAndTroops;
        EventsManager.OnGameTurnEnd += IncrementResourcesByTurn;
        EventsManager.OnGameTurnEnd += CPUTurn;
    }

    private void StartCPUGame()
    {
        totalSpotList = SpotManager.Instance.spotsList;
    }

    private void CPUTurn()
    {
        if (isSpotSetupTurn)
        {
            actionSpotTurn = Random.Range(minCPUTurnInterval, maxCPUTurnInterval);
            isSpotSetupTurn = false;
        }
        else
        {
            actionSpotTurn--;
            if (actionSpotTurn <= 0)
            {
                isSpotSetupTurn = true;
                StartCoroutine(CPUSpotTravelStart());
            }
        }

        if (isTroopsSetupTurn)
        {
            actionTroopsTurn = Random.Range(minCPUTroopsUpgradeInterval, maxCPUTroopsUpgradeInterval);
            isTroopsSetupTurn = false;
        }
        else
        {
            actionTroopsTurn--;
            if (actionTroopsTurn <= 0)
            {
                isTroopsSetupTurn = true;
                int randomUpgrade = Random.Range(1, 4);
                ExecuteTroopsUpgrade((TroopsManager.TroopsEspecializations)randomUpgrade);
            }
        }
    }

    IEnumerator CPUSpotTravelStart()
    {
        yield return null;

        if (cpuTotalResources >= GameManager.Instance.resourcesToTravel && cpuTotalSpots > 0)
        {
            int randomFromSpot = Random.Range(0, enemySpotList.Count - 1);
            int randomToSpot = Random.Range(0, totalSpotList.Count - 1);

            Spot fromSpot = enemySpotList[randomFromSpot];
            Spot toSpot = totalSpotList[randomToSpot];

            yield return null;

            //RANDOM SPOT UPGRADE//
            int randomUpgrading = Random.Range(0, 100);
            if(randomUpgrading % 2 == 0)
            {
                int randomUpgrade = Random.Range(0, 4);
                ExecuteSpotUpgrade(fromSpot, (SpotManager.SpotEspecialization)randomUpgrade);
            }
            ////////////////////////

            yield return null;

            if (fromSpot.spotTroops > 1)
            {
                TroopsManager.Instance.TroopsTravelStart(fromSpot, toSpot, fromSpot.SpotTroopsToTravel(), fromSpot.currentPlayerID);

                cpuTotalResources -= GameManager.Instance.resourcesToTravel;
            }
        }
    }

    private void RefreshSpotsAndTroops()
    {
        int countingSpots = 0;
        int countingTroops = 0;

        enemySpotList = new List<Spot>(); 

        foreach (var spot in totalSpotList)
        {
            if (spot.currentPlayerID == PlayerManager.Instance.enemyID)
            {
                countingSpots++;
                countingTroops += spot.spotTroops;
                enemySpotList.Add(spot);
            }
        }

        cpuTotalTroops = countingTroops;
        cpuTotalSpots = countingSpots;

        IncrementResourcesByTurn();
    }

    private void IncrementResourcesByTurn()
    {
        resourcesCounter++;

        if (resourcesCounter >= GameManager.Instance.defaultResourcesTurns && cpuTotalResources < GameManager.Instance.maxResources)
        {
            int turnResources = 0;

            //
            if (cpuTotalSpots >= 6)
                turnResources += cpuTotalSpots / 3;
            else
                turnResources++;
            //
            //TODO - Pegar o valor de quantidade de farms do player
            //turnResources += resourcesBySpotModifier;

            cpuTotalResources += turnResources;

            if (cpuTotalResources > GameManager.Instance.maxResources)
                cpuTotalResources = GameManager.Instance.maxResources;

            resourcesCounter = 0;
        }
    }

    //////////////
    ///UPGRADES///
    //////////////

    public void ExecuteSpotUpgrade(Spot spotToUpgrade, SpotManager.SpotEspecialization upgrade)
    {
        if (spotToUpgrade.ValidateUpgrade())
        {
            if (spotToUpgrade.currentEspecialization == SpotManager.SpotEspecialization.None)
            {
                spotToUpgrade.UpgradeSpot(upgrade);
            }
            else
            {
                //TODO - JÁ FOI ESPECIALIZADA
                Debug.LogWarning("CPU SPOT JA FOI ESPECIALIZADA");
            }
        }
        else
        {
            //TODO - SEM REQUERIMENTOS
            Debug.LogWarning("CPU SPOT NAO TEM OS REQUERIMENTOS");
        }
    }

    public void ExecuteTroopsUpgrade(TroopsManager.TroopsEspecializations newUpgrade)
    {
        if (ValidateCPUUpgrade())
        {
            if (currentUpgrade == TroopsManager.TroopsEspecializations.None)
            {
                UpgradeTroops(newUpgrade);
            }
            else
            {
                //TODO - JÁ FOI ESPECIALIZADA
                Debug.LogWarning("CPU JA FOI ESPECIALIZADA");
            }
        }
        else
        {
            //TODO - SEM REQUERIMENTOS
            Debug.LogWarning("CPU NAO TEM OS REQUERIMENTOS");
        }
    }

    public bool ValidateCPUUpgrade()
    {
        if (GameManager.Instance.resourcesToUpgradeTroops <= cpuTotalResources && GameManager.Instance.spotsToUpgradeTroops <= cpuTotalSpots)
            return true;

        return false;
    }


    public void UpgradeTroops(TroopsManager.TroopsEspecializations upgrade)
    {
        Debug.Log("CPU TROOPS UPGRADE COMPLETE: " + upgrade);
        currentUpgrade = upgrade;
        cpuTotalResources -= GameManager.Instance.resourcesToUpgradeTroops;

        switch (upgrade)
        {
            case TroopsManager.TroopsEspecializations.LandTroops:
                //EFFECT DIRECTLY ON TROOP
                HUDManager.Instance.SetFeedback("ENEMY LAND TROOPS UPGRADE COMPLETE");
                break;

            case TroopsManager.TroopsEspecializations.SeaTroops:
                //EFFECT DIRECTLY ON TROOP
                HUDManager.Instance.SetFeedback("ENEMY SEA TROOPS UPGRADE COMPLETE");
                break;

            case TroopsManager.TroopsEspecializations.StrongTroops:
                HUDManager.Instance.SetFeedback("ENEMY STRONG TROOPS UPGRADE COMPLETE");
                cpuTroopsStrengthModifier = 2;
                break;

            case TroopsManager.TroopsEspecializations.AvidTroops:
                HUDManager.Instance.SetFeedback("ENEMY AVID TROOPS UPGRADE COMPLETE");
                cpuTroopsToTravelModifier = 2;
                break;
        }

        EventsManager.EnemyTroopsUpgraded();
    }
}
