﻿using UnityEngine;
using System.Collections;
using TouchScript.Gestures;

public class SelectionDragController : MonoBehaviour {

    public static SelectionDragController Instance { get; set; }

    private GameObject pathContainer;
    private ReleaseGesture releaseGesture;
    public GameObject pathPrefab, dragTarget;
    public bool isDragging;

    void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        EventsManager.OnSelectDrag += StartDrag;
        EventsManager.OnSelectDragEnd += EndDrag;
    }

    void Update()
    {
        if(isDragging)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit = new RaycastHit();
            if (Physics.Raycast(ray, out hit))
            {
                if(hit.collider.tag == "Cell")
                {
                    PlaceDots(SpotManager.Instance.currentSelectedSpot.transform, hit.collider.transform);
                    dragTarget.transform.position = hit.collider.transform.position;
                    dragTarget.transform.rotation = hit.collider.transform.rotation;
                }
            }
        }
    }

    private void StartDrag()
    {
        isDragging = true;
        releaseGesture = SpotManager.Instance.currentSelectedSpot.GetComponent<ReleaseGesture>();
        dragTarget.SetActive(true);
    }

    private void EndDrag()
    {
        isDragging = false;
        Destroy(pathContainer);
        dragTarget.SetActive(false);

        Ray ray = Camera.main.ScreenPointToRay(releaseGesture.ScreenPosition);
        RaycastHit hit = new RaycastHit();
        if (Physics.Raycast(ray, out hit))
        {
            if (hit.collider.tag == "Cell")
            {
                Debug.Log(hit.collider.name);
                if (hit.collider.GetComponent<Spot>() != null && hit.collider.GetComponent<Spot>() != SpotManager.Instance.currentSelectedSpot)
                {
                    SpotManager.Instance.DestinationSpot(hit.collider.GetComponent<Spot>());
                    EventsManager.SpotUnselect();
                }
            }
        }
    }

    private void PlaceDots(Transform fromPosition, Transform toPosition)
    {
        float cellSize = 0.1f;
        float lineSize = Quaternion.Angle(toPosition.rotation, fromPosition.rotation) / (cellSize * 50);
        float rotationAngle = Quaternion.Angle(toPosition.rotation, fromPosition.rotation) / lineSize;

        Destroy(pathContainer);
        pathContainer = new GameObject();
        pathContainer.name = "PathContainer";

        for (int i = 0; i < lineSize; i++)
        {
            Quaternion targetPath = Quaternion.Euler(toPosition.parent.eulerAngles);
            GameObject currDot = GameObject.Instantiate(pathPrefab, Vector3.zero, fromPosition.parent.rotation) as GameObject;
            currDot.transform.rotation = Quaternion.RotateTowards(fromPosition.parent.rotation, targetPath, rotationAngle * i);
            currDot.GetComponentInChildren<MeshRenderer>().transform.localScale = Vector3.one * cellSize;
            currDot.GetComponentInChildren<MeshRenderer>().transform.localPosition = new Vector3(0, GridManager.Instance.rSize + 0.8f, 0);
            currDot.transform.parent = pathContainer.transform;
        }
    }
}
