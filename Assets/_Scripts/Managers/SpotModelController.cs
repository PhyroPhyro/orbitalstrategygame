﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpotModelController : MonoBehaviour {

    public List<GameObject> defaultModels = new List<GameObject>();
    public List<GameObject> farmModels = new List<GameObject>();
    public List<GameObject> fortressModels = new List<GameObject>();
    public List<GameObject> barracksModels = new List<GameObject>();

    public void SetSpotModel(SpotManager.SpotEspecialization currentUpgrade)
    {
        foreach (var model in defaultModels)
        {
            model.SetActive(false);
        }
        foreach (var model in farmModels)
        {
            model.SetActive(false);
        }
        foreach (var model in fortressModels)
        {
            model.SetActive(false);
        }
        foreach (var model in barracksModels)
        {
            model.SetActive(false);
        }

        StartCoroutine(ActivateModel(currentUpgrade));
    }

    IEnumerator ActivateModel(SpotManager.SpotEspecialization upgrade)
    {
        yield return null;

        switch (upgrade)
        {
            case SpotManager.SpotEspecialization.None:
                int randomDefModel = Random.Range(0, defaultModels.Count - 1);
                defaultModels[randomDefModel].SetActive(true);
                break;
            case SpotManager.SpotEspecialization.Farms:
                int randomFarmModel = Random.Range(0, farmModels.Count - 1);
                farmModels[randomFarmModel].SetActive(true);
                break;
            case SpotManager.SpotEspecialization.Fortress:
                int randomFortressModel = Random.Range(0, fortressModels.Count - 1);
                fortressModels[randomFortressModel].SetActive(true);
                break;
            case SpotManager.SpotEspecialization.Barracks:
                int randomBarracksModel = Random.Range(0, barracksModels.Count - 1);
                barracksModels[randomBarracksModel].SetActive(true);
                break;
        }
    }
}
