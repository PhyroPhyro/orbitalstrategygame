﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ResourcesManager : MonoBehaviour {

    public static ResourcesManager Instance { get; set; }
    public int currentResources;
    public int resourcesBySpotModifier;

    private int resourcesCounter;
    private int currentSpots;

    void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        EventsManager.OnGameTurnEnd += IncrementResourcesByTurn;
        EventsManager.OnTravelStart += ChargeByTravel;
    }

    private void IncrementResourcesByTurn()
    {
        resourcesCounter++;

        if (resourcesCounter >= GameManager.Instance.defaultResourcesTurns && currentResources < GameManager.Instance.maxResources)
        {
            resourcesCounter = 0;
            int turnResources = 0;

            //
            currentSpots = SpotManager.Instance.currentOwnSpots;
            if (currentSpots >= 6)
                turnResources += currentSpots / 3;
            else
                turnResources++;
            //
            resourcesBySpotModifier = 0;
            foreach (var spot in SpotManager.Instance.spotsList)
            {
                if (spot.currentPlayerID == PlayerManager.Instance.playerID && spot.currentEspecialization == SpotManager.SpotEspecialization.Farms)
                    resourcesBySpotModifier++;
            }
            turnResources += resourcesBySpotModifier;

            if (currentResources > GameManager.Instance.maxResources)
                currentResources = GameManager.Instance.maxResources;

            ChangeResourcesValueBy(turnResources);
        }  
    }

    public void ChangeResourcesValueBy(int value)
    {
        currentResources += value;
        EventsManager.ResourcesChanged();
    }

    private void ChargeByTravel()
    {
        ChangeResourcesValueBy(-GameManager.Instance.resourcesToTravel);
    }
}
