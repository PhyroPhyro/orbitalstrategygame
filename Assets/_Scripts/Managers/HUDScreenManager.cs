﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HUDScreenManager : MonoBehaviour {

    public static HUDScreenManager Instance { get; set; }
    public Text v_centralFeedback, h_centralFeedback;
    public GameObject verticalContainer, horizontalContainer, horizontalCamera, verticalCamera;

    void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        SetOrientation();

        v_centralFeedback.GetComponent<CanvasRenderer>().SetAlpha(0f);
        h_centralFeedback.GetComponent<CanvasRenderer>().SetAlpha(0f);
    }

    public void SetFeedback(string message)
    {
        v_centralFeedback.text = message;
        h_centralFeedback.text = message;

        v_centralFeedback.GetComponent<CanvasRenderer>().SetAlpha(1f);
        v_centralFeedback.CrossFadeAlpha(0.01f, 2f, false);

        h_centralFeedback.GetComponent<CanvasRenderer>().SetAlpha(1f);
        h_centralFeedback.CrossFadeAlpha(0.01f, 2f, false);
    }
 
    private void SetOrientation()
    {
        if(Screen.orientation == ScreenOrientation.Landscape)
        {
            verticalContainer.SetActive(false);
            horizontalContainer.SetActive(true);

            verticalCamera.SetActive(false);
            horizontalCamera.SetActive(true);
        }
        else
        {
            verticalContainer.SetActive(true);
            horizontalContainer.SetActive(false);

            verticalCamera.SetActive(true);
            horizontalCamera.SetActive(false);
        }
    }
}
