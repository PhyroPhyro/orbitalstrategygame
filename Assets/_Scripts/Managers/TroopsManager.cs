﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TroopsManager : MonoBehaviour {

    public static TroopsManager Instance { get; set; }

    private List<Spot> spotList;
    private List<Troop> activeTroopsList = new List<Troop>();
    public int startingTroops, especializationMinTroops, especializationMinSpots;
    public GameObject troopsPrefab;
    public int currentTotalTroops;
    private int troopsCounter;
    public int troopsStrengthModifier = 0;
    public int troopsToTravelModifier = 1;

    public enum TroopsEspecializations
    {
        None = 0,
        LandTroops = 1,
        SeaTroops = 2,
        StrongTroops = 3,
        AvidTroops = 4
    }
    public TroopsEspecializations currentEspecialization = TroopsEspecializations.None;

    void Awake()
    {
        Instance = this;
    }

	void Start ()
    {
        EventsManager.OnGameTurnEnd += RefeshTroops;
        EventsManager.OnSpotAttacked += RefeshTroops;
        EventsManager.OnTravelStart += RefeshTroops;
        EventsManager.OnTravelEnd += RefeshTroops;

        EventsManager.OnGameTurnEnd += GenerateTroopsByTurn;
        EventsManager.OnMapCreationEnd += StartGameTroops;
	}

    private void StartGameTroops()
    {
        spotList = SpotManager.Instance.spotsList;

        foreach (var spot in spotList)
        {
            spot.IncrementTroops (startingTroops);
        }
    }

    private void GenerateTroopsByTurn()
    {
        troopsCounter++;

        if (troopsCounter >= GameManager.Instance.defaultTroopTurns)
        {
            EventsManager.TroopsGenerated();
            troopsCounter = 0;
        }
    }

    private void RefeshTroops()
    {
        currentTotalTroops = 0;
        foreach (var spot in spotList)
        {
            if(spot.currentPlayerID == PlayerManager.Instance.playerID)
                currentTotalTroops += spot.spotTroops;
        }

        EventsManager.TroopsChanged();
    }

    public void TroopsTravelStart(Spot fromSpot, Spot toSpot, int troopsAmount, int ownerPlayerID)
    {
        GameObject currentTroop = GameObject.Instantiate(troopsPrefab);
        currentTroop.GetComponent<Troop>().SetupTroop(fromSpot, toSpot, troopsAmount, ownerPlayerID);
        activeTroopsList.Add(currentTroop.GetComponent<Troop>());
    }

    public bool ValidateUpgrade()
    {
        if (GameManager.Instance.resourcesToUpgradeTroops <= ResourcesManager.Instance.currentResources && GameManager.Instance.spotsToUpgradeTroops <= SpotManager.Instance.currentOwnSpots)
            return true;

        return false;
    }

    public void UpgradeTroops(TroopsEspecializations upgrade)
    {
        Debug.Log("TROOPS UPGRADE COMPLETE");
        currentEspecialization = upgrade;
        ResourcesManager.Instance.ChangeResourcesValueBy(-GameManager.Instance.resourcesToUpgradeTroops);

		switch (upgrade)
        {
            case TroopsEspecializations.LandTroops:
                //EFFECT DIRECTLY ON TROOP
                HUDManager.Instance.SetFeedback("LAND TROOPS UPGRADE COMPLETE");
                break;

            case TroopsEspecializations.SeaTroops:
                //EFFECT DIRECTLY ON TROOP
                HUDManager.Instance.SetFeedback("SEA TROOPS UPGRADE COMPLETE");
                break;

            case TroopsEspecializations.StrongTroops:
                HUDManager.Instance.SetFeedback("STRONG TROOPS UPGRADE COMPLETE");
                troopsStrengthModifier = 2;
                break;

            case TroopsEspecializations.AvidTroops:
                HUDManager.Instance.SetFeedback("AVID TROOPS UPGRADE COMPLETE");
                troopsToTravelModifier = 2;
                break;
        }

        EventsManager.TroopsUpgraded();
    }
}
