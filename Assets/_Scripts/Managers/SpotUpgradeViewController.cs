﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SpotUpgradeViewController : MonoBehaviour {

    public Text upgradeTitle, upgradeRequirements, upgradeBonus;
    private SpotManager.SpotEspecialization _selectedUpgrade;
    private Spot _currentSpot;

    public void SetupSpotUpgradeView(SpotManager.SpotEspecialization spotUpgrade, Spot currentSpot)
    {
        _selectedUpgrade = spotUpgrade;
        _currentSpot = currentSpot;

        switch (spotUpgrade)
        {
            case SpotManager.SpotEspecialization.Barracks:
                upgradeTitle.text = "BARRACKS";
                upgradeBonus.text = "DOUBLE TROOPS GENERATION ON THIS SPOT";
                break;

            case SpotManager.SpotEspecialization.Farms:
                upgradeTitle.text = "FARMS";
                upgradeBonus.text = "GENERATE 1 BONUS RESOURCE WHILE POCESS THIS SPOT";
                break;

            case SpotManager.SpotEspecialization.Fortress:
                upgradeTitle.text = "FORTRESS";
                upgradeBonus.text = "REQUIRES MORE TROOPS TO TAKE TROOPS OUT OF THIS SPOT";
                break;
        }

        upgradeRequirements.text = "REQUIREMENTS: \n" + GameManager.Instance.resourcesToUpgradeSpots + " RESOURCES \n" + GameManager.Instance.troopsToUpgradeSpots + " TROOPS";
    }

    public void ExecuteUpgrade()
    {
        if (_currentSpot.ValidateUpgrade())
        {
            if (_currentSpot.currentEspecialization == SpotManager.SpotEspecialization.None)
            {
                _currentSpot.UpgradeSpot(_selectedUpgrade);
            }
            else
            {
                //TODO - JÁ FOI ESPECIALIZADA
                HUDManager.Instance.SetFeedback("ALREADY UPGRADED");
                Debug.LogWarning("JA FOI ESPECIALIZADA");
                return;
            }
        }
        else
        {
            //TODO - SEM REQUERIMENTOS
            HUDManager.Instance.SetFeedback("DON'T MEET REQUIREMENTS");
            Debug.LogWarning("NAO TEM OS REQUERIMENTOS");
            return;
        }

        EventsManager.SpotUpgraded();
        EventsManager.SpotUnselect();
    }
}
