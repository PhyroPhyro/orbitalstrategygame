﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerManager : MonoBehaviour {

    public static PlayerManager Instance { get; set; }
    public List<Color> playersColors = new List<Color>();
    public int playerID;
    public int enemyID;

    void Awake()
    {
        Instance = this;
    }
}
