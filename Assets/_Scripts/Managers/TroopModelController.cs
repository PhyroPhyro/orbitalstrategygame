﻿using UnityEngine;
using System.Collections;

public class TroopModelController : MonoBehaviour {

    public GameObject landModel, seaModel;
    private Troop parentTroop;

    void Start()
    {
        parentTroop = transform.parent.GetComponent<Troop>();
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.transform.parent.GetComponent<Cell>())
        {
            if(other.transform.parent.GetComponent<Cell>().cellState != parentTroop.currentCell)
            {
                parentTroop.VerifyTerrain(other.transform.parent.GetComponent<Cell>().cellState);
                SwapTroopModel();
            }
        }
    }

    private void SwapTroopModel()
    {
        landModel.SetActive(false);
        seaModel.SetActive(false);

        switch(parentTroop.currentCell)
        {
            case GridManager.CellStates.Water:
                seaModel.SetActive(true);
                break;
            default:
                landModel.SetActive(true);
                break;
        }
    }
}
