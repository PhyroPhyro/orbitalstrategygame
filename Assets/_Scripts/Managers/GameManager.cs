﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {

    public static GameManager Instance { get; set; }
    public bool isOnline = false;

    //MAP//
    [Header("Map")]
    public int islandRange;
    public int islandAmount;
    public int populatedTerrain;

    //SPOTS//
    [Header("Spots")]
    public int spotsAmount;
    public int troopsToUpgradeSpots;
    public int resourcesToUpgradeSpots;
    public Color selectedSpotColor, enemySpotColor, friendlySpotColor, unownedSpotColor;

    //TROOPS//
    [Header("Troops")]
    public int defaultTroopModifier;
    public int defaultTroopTurns;
    public int defaultTroopStrength;
    public int defaultTroopSpeed;
    public int spotsToUpgradeTroops;
    public int resourcesToUpgradeTroops;
    public int troopsSpeedModifier;

    //TURNS//
    [Header("Turns")]
    public bool isGameStarted;
    public float turnLenght;
    private float startTime;
    private float frameTime;
    private int frameCount;

    //PATHS//
    [Header("Paths")]
    public Color transferingColor;
    public Color enemyColor;

    //RESOURCES//
    [Header("Resources")]
    public int defaultResourcesTurns;
    public int resourcesToTravel;
    public int maxResources;

    void Awake()
    {
        Instance = this;
        startTime = Time.time;
    }

    void Start()
    {
        EventsManager.OnMapCreationEnd += StartGame;
    }

    private void StartGame()
    {
        isGameStarted = true;
    }

    // Update is called once per frame
    void Update() {
        if(isGameStarted)
        {
            Time.timeScale = 1;
            frameCount++;
            frameTime = Time.time - startTime;
            if (frameTime >= turnLenght)
            {
                frameCount = 0;
                frameTime = 0;
                startTime = Time.time;
                EventsManager.GameTurnEnd();
            }
        }
	}
}
