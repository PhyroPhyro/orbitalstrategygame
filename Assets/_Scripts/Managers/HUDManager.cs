﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HUDManager : MonoBehaviour
{

    public static HUDManager Instance { get; set; }

    public Text troopsText, spotsText, resourcesText, currentSpotTroops, currentSpotType, myTroopsType, enemyTroopsType;
    public Button troopsUpgradesButton, troopsUpgradesButtonDeactivated;
    public GameObject spotSelectedContainer, spotUnselectedContainer, buttonsContainer, upgradeMenu;

    void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        EventsManager.OnResourcesChanged += SetResourcesText;
        EventsManager.OnSpotChangeOwner += SetSpotsText;
        EventsManager.OnTroopsChanged += SetTroopsText;
        EventsManager.OnTroopsChanged += SetCurrentSpotText;
        EventsManager.OnSpotSelect += SetCurrentSpotText;
        EventsManager.OnSpotSelect += ActivateUpgradeButtons;
        EventsManager.OnSpotSelect += SelectedSlot;
        EventsManager.OnSpotUnselect += UnselectedSlot;
        EventsManager.OnSpotUnselect += DeactivateUpgradeButtons;
        EventsManager.OnSpotUpgraded += DeactivateUpgradeButtons;
        EventsManager.OnSpotUpgraded += SetCurrentSpotText;
        EventsManager.OnTroopsUpgraded += SetMyTroopsType;
        EventsManager.OnEnemyTroopsUpgraded += SetEnemyTroopsType;
        EventsManager.OnTroopsUpgraded += LockTroopUpgrades;
    }

    private void LockTroopUpgrades()
    {
        troopsUpgradesButton.gameObject.SetActive(false);
        troopsUpgradesButtonDeactivated.gameObject.SetActive(true);
    }

    public void DeactivatedUpgradesFeedback()
    {
        SetFeedback("ALREADY UPGRADED TROOPS");
    }

    public void SetTroopsText()
    {
        troopsText.text = TroopsManager.Instance.currentTotalTroops.ToString() + " TROOPS";
    }

    public void SetSpotsText()
    {
        spotsText.text = SpotManager.Instance.currentOwnSpots.ToString() + "/" + GameManager.Instance.spotsAmount + " SPOTS";
    }

    public void SetResourcesText()
    {
        resourcesText.text = ResourcesManager.Instance.currentResources.ToString() + "/" + GameManager.Instance.maxResources + " RESOURCES";
    }

    public void SetMyTroopsType()
    {
        myTroopsType.text = TroopsManager.Instance.currentEspecialization.ToString().ToUpper();
    }

    public void SetEnemyTroopsType()
    {
        enemyTroopsType.text = CPUBehaviourManager.Instance.currentUpgrade.ToString().ToUpper();
    }

    public void SetCurrentSpotText()
    {
        if (SpotManager.Instance.currentSelectedSpot != null)
        {
            currentSpotTroops.text = SpotManager.Instance.currentSelectedSpot.spotTroops + "Troops";
            currentSpotType.text = SpotManager.Instance.currentSelectedSpot.currentEspecialization.ToString();
        }
    }

    private void SelectedSlot()
    {
        spotSelectedContainer.SetActive(true);
        spotUnselectedContainer.SetActive(false);
    }

    private void UnselectedSlot()
    {
        upgradeMenu.SetActive(false);
        spotSelectedContainer.SetActive(false);
        spotUnselectedContainer.SetActive(true);
    }

    private void ActivateUpgradeButtons()
    {
        if (SpotManager.Instance.currentSelectedSpot.currentEspecialization == SpotManager.SpotEspecialization.None)
            buttonsContainer.SetActive(true);
    }

    private void DeactivateUpgradeButtons()
    {
        upgradeMenu.SetActive(false);
        buttonsContainer.SetActive(false);
    }

    public void SetFeedback(string message)
    {
        HUDScreenManager.Instance.SetFeedback(message);
    }
}