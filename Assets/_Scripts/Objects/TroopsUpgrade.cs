﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TroopsUpgrade : MonoBehaviour {

    public TroopsManager.TroopsEspecializations buttonUpgrade;
    public Text requirementsText;

    void Start()
    {
        requirementsText.text = GameManager.Instance.resourcesToUpgradeTroops.ToString() + " RESOURCES/" + GameManager.Instance.spotsToUpgradeTroops.ToString() + " SPOTS";
    }

    public void ExecuteUpgrade()
    {
        if (TroopsManager.Instance.ValidateUpgrade())
        {
            if (TroopsManager.Instance.currentEspecialization == TroopsManager.TroopsEspecializations.None)
            {
                TroopsManager.Instance.UpgradeTroops(buttonUpgrade);
                transform.parent.gameObject.SetActive(false);
            }
            else
            {
                //TODO - JÁ FOI ESPECIALIZADA
                HUDManager.Instance.SetFeedback("ALREADY UPGRADED");
                Debug.LogWarning("JA FOI ESPECIALIZADA");
            }
        }
        else
        {
            //TODO - SEM REQUERIMENTOS
            HUDManager.Instance.SetFeedback("DON'T MEET REQUIREMENTS");
            Debug.LogWarning("NAO TEM OS REQUERIMENTOS");
        }
    }
}
