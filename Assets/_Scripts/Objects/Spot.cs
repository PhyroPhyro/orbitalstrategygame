﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using TouchScript.Gestures;
using System;

public class Spot : MonoBehaviour {

    public int currentPlayerID = 0;
    private int lastAttackingPlayerID = 0;
    private Color currentPlayerColor;
    private SpotUpgrade highlightedButton;

    public SpotManager.SpotEspecialization currentEspecialization = SpotManager.SpotEspecialization.None;
    public int spotTroops;
    public int generatedTroopsModifier = 0;
    public int spotDefenseModifier = 0;
    public TextMesh spotTroopsText;
    public GameObject spotModel, sendTroopsModel;
    public bool isSelectedSpot, canSendTroopsTo;
    public List<GameObject> upgradeButtons;
    public SpotModelController modelContainer;

    void Start()
    {
        EventsManager.OnSpotAttacked += CheckSpotOwner;

        EventsManager.OnSpotChangeOwner += ReturnSpotColor;
        EventsManager.OnSpotUnselect += ReturnSpotColor;

        EventsManager.OnSpotAttacked += RefreshTroopsText;
        EventsManager.OnGameTurnEnd += RefreshTroopsText;
        EventsManager.OnTravelStart += RefreshTroopsText;

        EventsManager.OnTroopsGenerated += IncrementTroopsByTurn;

        modelContainer.SetSpotModel(SpotManager.SpotEspecialization.None);
    }

    private void RefreshTroopsText()
    {
        spotTroopsText.text = spotTroops.ToString();
    }

    public void IncrementTroopsByTurn()
    {
        if(currentPlayerID != 0)
        {
            spotTroops += GameManager.Instance.defaultTroopModifier + generatedTroopsModifier;
        }
    }

    public void IncrementTroops(int amount)
    {
        if(currentPlayerID != 0)
            spotTroops += amount;
    }

    public void SelectSpot(bool isSelected)
    {
        if (isSelected)
        {
            spotModel.GetComponent<MeshRenderer>().material.color = GameManager.Instance.selectedSpotColor;
        }
        else
        {
            sendTroopsModel.SetActive(true);

            if (currentPlayerID == PlayerManager.Instance.playerID)
                sendTroopsModel.GetComponent<MeshRenderer>().material.color = GameManager.Instance.friendlySpotColor;
            else if (currentPlayerID == 0)
                sendTroopsModel.GetComponent<MeshRenderer>().material.color = GameManager.Instance.unownedSpotColor;
            else
                sendTroopsModel.GetComponent<MeshRenderer>().material.color = GameManager.Instance.enemySpotColor;
        }

        isSelectedSpot = isSelected;
        canSendTroopsTo = !isSelected;
    }

    public void UnselectSpot()
    {
        sendTroopsModel.SetActive(false);
        isSelectedSpot = false;
        canSendTroopsTo = false;
        ReturnSpotColor();
    }

    public int SpotTroopsToTravel()
    {
        int troopsToTravel = 0;
        if(!GameManager.Instance.isOnline)
        {
            if(currentPlayerID != PlayerManager.Instance.playerID)
            {
                if (CPUBehaviourManager.Instance.cpuTroopsToTravelModifier > 1)
                    troopsToTravel = (spotTroops / 2) + (spotTroops / CPUBehaviourManager.Instance.cpuTroopsToTravelModifier);
                else
                    troopsToTravel = spotTroops / 2;
            }
            else
            {
                if (TroopsManager.Instance.troopsToTravelModifier > 1)
                    troopsToTravel = (spotTroops / 2) + (spotTroops / TroopsManager.Instance.troopsToTravelModifier);
                else
                    troopsToTravel = spotTroops / 2;
            }
        }
        else
        {
            //TODO - ONLINE TROOPS TO TRAVEL
        }

        spotTroops -= troopsToTravel;
        return troopsToTravel;
    }

    public void ReceiveTroops(Troop troop)
    {
        if (troop.ownerPlayerID == currentPlayerID)
        {
            IncrementTroops(troop.troopAmount);
        }
        else
        {
            lastAttackingPlayerID = troop.ownerPlayerID;
            StartCoroutine(DefendTroops(troop.troopAmount, troop.troopStrength));
        }

        EventsManager.TravelEnd();
    }

    IEnumerator DefendTroops(int amount, int strength)
    {
        yield return null;
        if (strength > 1)
        {
            int newAmount = amount + (amount / strength);
            spotTroops -= amount + newAmount;
        }
        else
        {
            spotTroops -= amount;
        }
        ///////
        if (spotDefenseModifier > 0)
        {
            int defendedAmount = amount / spotDefenseModifier;
            spotTroops += defendedAmount;
        }
        yield return null;
        EventsManager.SpotAttacked();
    }

    private void CheckSpotOwner()
    {
        if (spotTroops <= 0 && lastAttackingPlayerID != 0)
        {
            SpotManager.Instance.ChangeSpotOwner(this, lastAttackingPlayerID);
            spotTroops = spotTroops * -1;
        }
    }

    public void ChangeThisSpotOwner(int _playerID)
    {
        currentPlayerID = _playerID;
        currentPlayerColor = PlayerManager.Instance.playersColors[_playerID - 1];
    }

    private void ReturnSpotColor()
    {
        if(!isSelectedSpot)
        {
            if (currentPlayerID != 0)
                spotModel.GetComponent<MeshRenderer>().material.color = currentPlayerColor;
            else
                spotModel.GetComponent<MeshRenderer>().material.color = GameManager.Instance.unownedSpotColor;
        }
    }

    public bool ValidateUpgrade()
    {
        if (GameManager.Instance.resourcesToUpgradeSpots <= ResourcesManager.Instance.currentResources && GameManager.Instance.troopsToUpgradeSpots <= spotTroops && currentEspecialization == SpotManager.SpotEspecialization.None)
            return true;

        return false;
    }

    public void UpgradeSpot(SpotManager.SpotEspecialization upgrade)
    {
        currentEspecialization = upgrade;
        modelContainer.SetSpotModel(upgrade);
        ResourcesManager.Instance.ChangeResourcesValueBy(-GameManager.Instance.resourcesToUpgradeSpots);

        switch (upgrade)
        {
            case SpotManager.SpotEspecialization.Barracks:
                //RecoursesManager does the job
                generatedTroopsModifier++;
                break;

            case SpotManager.SpotEspecialization.Farms:
                break;

            case SpotManager.SpotEspecialization.Fortress:
                spotDefenseModifier = 2;
                break;
        }
    }

    //TOUCH//
    private void OnEnable()
    {
        GetComponent<LongPressGesture>().LongPressed += longPressHandler;
        GetComponent<ReleaseGesture>().Released += endLongPressHandler;
    }

    private void OnDisable()
    {
        GetComponent<LongPressGesture>().LongPressed -= longPressHandler;
        GetComponent<ReleaseGesture>().Released-= endLongPressHandler;
    }

    private void longPressHandler(object sender, EventArgs eventArgs)
    {
        if (currentPlayerID == PlayerManager.Instance.playerID || SpotManager.Instance.currentSelectedSpot != null)
        {
            SpotManager.Instance.SelectSpot(this);
            EventsManager.SelectDrag();
        }

        SpotManager.Instance.wasTapped = false;
    }

    private void endLongPressHandler(object sender, EventArgs eventArgs)
    {
        if(!SpotManager.Instance.wasTapped)
            EventsManager.SelectDragEnd();
    }
}

