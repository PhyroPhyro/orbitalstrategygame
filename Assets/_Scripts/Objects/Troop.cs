﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Troop : MonoBehaviour {

    private Transform toSpotTransform;
    public Spot fromSpot, toSpot;
    public TextMesh troopAmountText;
    public GameObject troopModel;
    public int ownerPlayerID, troopAmount, baseTroopSpeed, troopsSpeed, troopStrength;
    public GridManager.CellStates currentCell;
	private bool hasArrived = false;

    void Start()
    {
        transform.rotation = fromSpot.gameObject.transform.parent.rotation;
        toSpotTransform = toSpot.gameObject.transform.parent;
    }

    public void SetupTroop(Spot _fromSpot, Spot _toSpot, int _troopAmount, int _ownerPlayerID)
    {
        fromSpot = _fromSpot;
        toSpot = _toSpot;
        troopAmount = _troopAmount;
        ownerPlayerID = _ownerPlayerID;

        if(PlayerManager.Instance.playerID == ownerPlayerID)
        {
            troopAmountText.transform.parent.GetComponent<SpriteRenderer>().color = PlayerManager.Instance.playersColors[0];
            troopStrength = GameManager.Instance.defaultTroopStrength + TroopsManager.Instance.troopsStrengthModifier;
            baseTroopSpeed = GameManager.Instance.defaultTroopSpeed;
            troopsSpeed = baseTroopSpeed;
        }
        else
        {
            troopAmountText.transform.parent.GetComponent<SpriteRenderer>().color = PlayerManager.Instance.playersColors[1];

            if (!GameManager.Instance.isOnline)
            {
                troopStrength = GameManager.Instance.defaultTroopStrength + CPUBehaviourManager.Instance.cpuTroopsStrengthModifier;
                baseTroopSpeed = GameManager.Instance.defaultTroopSpeed;
            }
            else
            {
                //TODO - TROOP BEHAVIOUR ON ONLINE ENEMY
            }
        }

        troopsSpeed = baseTroopSpeed;

        float cellSize = 0.3f;
        troopModel.transform.localPosition = new Vector3(0, GridManager.Instance.rSize + 0.35f, 0);
        troopModel.transform.localScale = Vector3.one * cellSize;
        troopAmountText.text = troopAmount.ToString();
        troopAmountText.transform.parent.localScale = Vector3.one * cellSize * 2;
        troopAmountText.transform.parent.localPosition = new Vector3(0, GridManager.Instance.rSize + 0.65f, 0);
    }

    void Update()
    {
        transform.rotation = Quaternion.RotateTowards(transform.rotation, toSpotTransform.rotation, Time.fixedDeltaTime * troopsSpeed);
        troopModel.transform.LookAt(toSpot.spotModel.transform);
        troopModel.transform.localRotation = Quaternion.Euler(0, troopModel.transform.localEulerAngles.y, 0);
        if (transform.rotation == toSpotTransform.rotation && !hasArrived)
		{
			toSpot.ReceiveTroops(this);
			hasArrived = true;
			StartCoroutine (FinalizeTroop ());
		}
    }

    public void VerifyTerrain(GridManager.CellStates thisCellState)
    {
        currentCell = thisCellState;
        //TODO - DEFINE TROOP SPEED BY TERRAIN
        if(!GameManager.Instance.isOnline)
        {
            if(ownerPlayerID == PlayerManager.Instance.playerID)
            {
                switch (thisCellState)
                {
                    case GridManager.CellStates.Water:
                        if (TroopsManager.Instance.currentEspecialization == TroopsManager.TroopsEspecializations.SeaTroops)
                            troopsSpeed = baseTroopSpeed + GameManager.Instance.troopsSpeedModifier;
                        else
                            troopsSpeed = baseTroopSpeed;
                        break;
                    default:
                        if (TroopsManager.Instance.currentEspecialization == TroopsManager.TroopsEspecializations.LandTroops)
                            troopsSpeed = baseTroopSpeed + GameManager.Instance.troopsSpeedModifier;
                        else
                            troopsSpeed = baseTroopSpeed;
                        break;
                }
            }
            else
            {
                switch (thisCellState)
                {
                    case GridManager.CellStates.Water:
                        if (CPUBehaviourManager.Instance.currentUpgrade == TroopsManager.TroopsEspecializations.SeaTroops)
                            troopsSpeed = baseTroopSpeed + GameManager.Instance.troopsSpeedModifier;
                        else
                            troopsSpeed = baseTroopSpeed;
                        break;
                    default:
                        if (CPUBehaviourManager.Instance.currentUpgrade == TroopsManager.TroopsEspecializations.LandTroops)
                            troopsSpeed = baseTroopSpeed + GameManager.Instance.troopsSpeedModifier;
                        else
                            troopsSpeed = baseTroopSpeed;
                        break;
                }
            }
        }
        else
        {
            //TODO - TROOP SPEED ON ONLINE MODE
        }
        
    }

    IEnumerator FinalizeTroop()
	{
		foreach (var meshs in GetComponentsInChildren<MeshRenderer>()) {
			meshs.enabled = false;
		}
		yield return null;
		Destroy(gameObject);
	}
}
