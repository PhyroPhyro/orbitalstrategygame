﻿using UnityEngine;
using System.Collections;

public class Path : MonoBehaviour {

    public bool isEnemy;
	public int troopsOnPath = 1;

    public void TroopArrived()
    {
		troopsOnPath -= 1;

		if(troopsOnPath <= 0)
	        StartCoroutine(UnplaceDots());
    }

    IEnumerator UnplaceDots()
    {
        foreach (var dot in GetComponentsInChildren<MeshRenderer>())
        {
			Color alphaColor = new Color (dot.material.color.r, dot.material.color.g, dot.material.color.b, 0);
			dot.material.color = alphaColor;
            yield return null;
        }

        Destroy(gameObject);
    }

    IEnumerator Start()
    {
        yield return null;
        StartCoroutine(AnimatePath());
    }

    IEnumerator AnimatePath()
    {
        Color animationColor;

        if (isEnemy)
            animationColor = GameManager.Instance.enemyColor;
        else
            animationColor = GameManager.Instance.transferingColor;

        Color currentColor = new Color(1, 1, 1);

        foreach (var dot in GetComponentsInChildren<MeshRenderer>())
        {
            if (dot.material.color ==  new Color(1, 1, 1))
                currentColor = animationColor;
        }

        foreach (var dot in GetComponentsInChildren<MeshRenderer>())
        {
            dot.material.color = currentColor;
            yield return new WaitForSeconds(0.15f);
        }

        StartCoroutine(AnimatePath());
    }
}
