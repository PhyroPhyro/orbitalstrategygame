﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Cell : MonoBehaviour {

    public GridManager.CellStates cellState;
    public List<GameObject> cellModels = new List<GameObject>();
    public List<GameObject> terrainModels = new List<GameObject>();
    public int cellX;
    public int cellY;

    public void SetCellState(GridManager.CellStates state)
    {
        cellState = state;

        foreach (var model in cellModels)
        {
            model.SetActive(false);
        }

        if (state != GridManager.CellStates.Land)
        {
            cellModels[(int)state].SetActive(true);

            if(state == GridManager.CellStates.Spot)
                cellModels[(int)state].transform.localPosition = new Vector3(0, GridManager.Instance.rSize + 0.06f, 0);
        }
        else
        {
            int randomTerrain = Random.Range(0, terrainModels.Count);
            //if (GameManager.Instance.populatedTerrain <= 0)
                randomTerrain = 0;

            GameObject terrain = terrainModels[randomTerrain];
            terrain.SetActive(true);

            //GameManager.Instance.populatedTerrain--;
        }


    }
}
