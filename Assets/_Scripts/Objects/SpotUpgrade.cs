﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SpotUpgrade : MonoBehaviour {

    private Spot currentSpot;
    public Vector3 maxScale;
    public SpotUpgradeViewController viewController;
    public SpotManager.SpotEspecialization buttonUpgrade;

    public void OpenButton()
    {
        gameObject.SetActive(true);
        iTween.ScaleTo(gameObject, new Hashtable
        {
            { "scale", maxScale },
            {"time", 1f},
            {"easetype", "easeOutElastic"}
        });
    }

    public void CloseButton()
    {
        iTween.ScaleTo(gameObject, new Hashtable
        {
            { "scale", Vector3.zero },
            {"time", 0.3f},
            {"easetype", "easeInBack" },
        });
    }

    void OnEnable()
    {
        currentSpot = SpotManager.Instance.currentSelectedSpot;
    }

    public void OnClick()
    {
        viewController.SetupSpotUpgradeView(buttonUpgrade, currentSpot);
        viewController.gameObject.SetActive(true);
    }
}
