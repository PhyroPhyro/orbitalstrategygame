﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class LoadNextScene : MonoBehaviour {

    public bool loadOnStart = false;
    public string sceneToLoad;

	void Start()
    {
        if (loadOnStart)
            SceneManager.LoadSceneAsync(sceneToLoad);
    }
}
