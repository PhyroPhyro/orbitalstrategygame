﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class SelectOrientation : MonoBehaviour {

	public void SetOrientationAndPlay(bool isVertical)
    {
        if(isVertical)
        {
            Screen.orientation = ScreenOrientation.Portrait;
        }
        else
        {
            Screen.orientation = ScreenOrientation.Landscape;
        }

        SceneManager.LoadScene("Loading");
    }
}
