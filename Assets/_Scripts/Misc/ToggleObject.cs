﻿using UnityEngine;
using System.Collections;

public class ToggleObject : MonoBehaviour {

    public GameObject CloseObj;
    public bool toOpen;

	public void OnClick()
    {
        CloseObj.SetActive(toOpen);
    }
}
