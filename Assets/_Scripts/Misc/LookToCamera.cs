﻿using UnityEngine;
using System.Collections;

public class LookToCamera : MonoBehaviour {

	void Update () {
        gameObject.transform.LookAt(Camera.main.transform);
	}
}
