﻿using UnityEngine;
using System.Collections;
using TouchScript.Gestures;
using System;

public class DeactivadeSpots : MonoBehaviour {

    //TOUCH//
    private void OnEnable()
    {
        GetComponent<TapGesture>().Tapped += tappedHandler;
    }

    private void OnDisable()
    {
        GetComponent<TapGesture>().Tapped -= tappedHandler;
    }

    private void tappedHandler(object sender, EventArgs eventArgs)
    {
        EventsManager.SpotUnselect();
    }

}
