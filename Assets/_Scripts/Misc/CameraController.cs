﻿using UnityEngine;
using System.Collections;
using TouchScript.Gestures;

public class CameraController : MonoBehaviour {

    public ScreenTransformGesture ManipulationGesture;
    public Rigidbody camPivot;
    public float onDragThreshold;

    private void OnEnable()
    {
        ManipulationGesture.Transformed += RotationRoutine;
    }

    private void OnDisable()
    {
        ManipulationGesture.Transformed -= RotationRoutine;
    }

    void Update()
    {
        if(SelectionDragController.Instance.isDragging)
        {
            float onDragSpeed = 0;
            if(Input.mousePosition.x > Screen.width - onDragThreshold * Screen.width)
            {
                onDragSpeed = -5;
            }
            else if(Input.mousePosition.x < onDragThreshold * Screen.width)
            {
                onDragSpeed = 5;
            }
            else
            {
                onDragSpeed = 0;
            }


            var rotation = Quaternion.Euler(0, onDragSpeed, 0);
            camPivot.transform.localRotation *= rotation;
        }
    }

    private void RotationRoutine(object sender, System.EventArgs e)
    {
        if(!SelectionDragController.Instance.isDragging)
        {
            float rotateAmount = Mathf.Clamp(-ManipulationGesture.DeltaPosition.x / (Screen.dpi * .5f), -2f, 2f);
            camPivot.AddRelativeTorque(new Vector3(0, rotateAmount * -50, 0));
        }
    }
}
